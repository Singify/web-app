import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Song } from '../../models/song';

@Component({
  selector: 'participant',
  template: `
    <div class="Participant o-grid o-grid-axis-align--end o-grid-cross-align--center">
      <div class="Participant__info o-grid__child u-8of12">
        <span class="s3 o-brand-title [ u-text-truncate u-color-primary u-margin-c ]">{{song.client.name}}</span>
        <h1 class="t1 u-text-truncate u-font-display u-margin-c"><strong>{{song.name}}</strong></h1>
        <span class="s3 u-text-truncate o-brand-title u-margin-c">{{song.artist}}</span>
      </div>
      <div class="Participant__photo o-grid__child u-4of12">
        <img src="{{song.image}}" alt="">
        <div class="Participant__cancel">
          <button (click)="handleSelectionRemoval(song.id)" class="Participant__cancel__btn"><i class="o-icon-small o-icon-close"></i></button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./participant.component.scss']
})
export class ParticipantComponent implements OnInit {
  @Input() song: Song;
  @Output() removeSelection = new EventEmitter();

  constructor() {

  }

  handleSelectionRemoval(id: number) {
    this.removeSelection.emit(id);
  }

  ngOnInit() {
  }

}
