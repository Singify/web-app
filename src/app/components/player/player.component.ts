import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
import { KaraokeService } from '../../services/karaoke/karaoke.service';
import { AuthService } from '../../services/auth/auth.service';
import { Song } from '../../models/song';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  playing: Song;
  isPlaying: boolean;
  isAuth: boolean;

  constructor(private karaokeProvider: KaraokeService, private authProvider: AuthService) {
    this.isAuth = localStorage.getItem('stfyToken') ? true : false;
    this.subscription = karaokeProvider.getPlaying().subscribe((p) => {
      this.playing = p;
      this.isPlaying = false;
    });
  }

  ngOnInit() {
    this.karaokeProvider.fetchPlaying();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  play() {
    this.karaokeProvider.play();
    this.isPlaying = true;
  }

  pause() {
    this.karaokeProvider.pause();
    this.isPlaying = false;
  }

  playNext() {
    this.karaokeProvider.playNext();
  }

  authWithSpotify(token: string) {
    this.isAuth = this.authProvider.authWithSpotify(token);
  }
}
