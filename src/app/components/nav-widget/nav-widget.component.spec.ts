import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavWidgetComponent } from './nav-widget.component';

describe('NavWidgetComponent', () => {
  let component: NavWidgetComponent;
  let fixture: ComponentFixture<NavWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
