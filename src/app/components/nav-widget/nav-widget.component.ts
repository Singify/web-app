import { Component } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'nav-widget',
  template: `
    <nav (click)="toggle()" class="Widget [ o-grid--vertical o-grid-cross-align--end o-grid-wrap--no ]" [ngClass]="{'is-open': isOpen}">
      <ul class="Widget__navigation">
        <li><a routerLink='/dashboard/bar-menu' routerLinkActive='is-active' class="navigation__link o-brand-title">Bar Menu</a></li>
        <li><a routerLink='/dashboard/reservations' routerLinkActive='is-active' class="navigation__link o-brand-title">Reservations</a></li>
        <li><a routerLink='/dashboard/orders' routerLinkActive='is-active' class="navigation__link o-brand-title">Orders</a></li>
        <li><a routerLink='/dashboard/karaoke' routerLinkActive='is-active' class="navigation__link o-brand-title">Karaoke</a></li>
        <li><a routerLink='/dashboard/stats' routerLinkActive='is-active' class="navigation__link o-brand-title">Stats</a></li>
      </ul>
      <button (click)="doLogout()" class="Widget__main-action [ o-grid o-grid-wrap--no o-grid-cross-align--center ]">
        <i class="o-icon-circle-arrow-left o-icon-small"></i>
        <strong class="u-block u-margin-m--left u-font-display">Sign out</strong>
      </button>
    </nav>
  `,
  styleUrls: ['./nav-widget.component.scss']
})
export class NavWidgetComponent {

  isOpen: boolean;

  constructor(private authProvider: AuthService) {
    this.isOpen = false;
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  doLogout() {
    this.authProvider.doLogout();
  }

}
