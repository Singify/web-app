import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
import { KaraokeService } from '../../services/karaoke/karaoke.service';

import { Song } from '../../models/song';

@Component({
  selector: 'karaoke-widget',
  template: `
    <div class="Widget [ o-grid o-grid-wrap--no o-grid-cross-align--center ]">
      <participant *ngFor="let song of queue" [song]="song" (removeSelection)="removeSelection($event)"></participant>
    </div>
  `,
  styleUrls: ['./karaoke-widget.component.scss']
})
export class KaraokeWidgetComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  queue: Song[];

  constructor(private karaokeProvider: KaraokeService) {
    this.subscription = karaokeProvider.getQueue().subscribe((q) => {
      this.queue = q;
    });
  }

  ngOnInit() {
    this.karaokeProvider.fetchQueue();
  }

  removeSelection(event: any) {
    this.karaokeProvider.removeSong(event);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
