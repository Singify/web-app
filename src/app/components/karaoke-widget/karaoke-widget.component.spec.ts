import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KaraokeWidgetComponent } from './karaoke-widget.component';

describe('KaraokeWidgetComponent', () => {
  let component: KaraokeWidgetComponent;
  let fixture: ComponentFixture<KaraokeWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KaraokeWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KaraokeWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
