import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
import { KaraokeService } from '../../services/karaoke/karaoke.service';

import { Song } from '../../models/song';

@Component({
  selector: 'player-widget',
  template: `
    <div class="Widget [ o-grid o-grid-content-align--center ]">
      <div *ngIf="playing" class="o-grid__child o-grid-align-self--center u-4of12 u-6of12@tab u-12of12@lap">
        <img class="Widget__cover" src="{{playing.image}}" alt="">
      </div>
      <div *ngIf="playing" class="o-grid__child o-grid-align-self--center u-8of12 u-6of12@tab u-12of12@lap">
        <h1 class="Widget__song t2 [ u-font-display u-margin-c u-text-truncate ]">{{playing.name}}</h1>
        <span class="Widget__artist s1 u-text-truncate">{{playing.artist}}</span>
      </div>
    </div>
  `,
  styleUrls: ['./player-widget.component.scss']
})
export class PlayerWidgetComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  playing: Song;

  constructor(private karaokeProvider: KaraokeService) {
    this.subscription = karaokeProvider.getPlaying().subscribe((q) => {
      this.playing = q;
    });
  }

  ngOnInit() {
    this.karaokeProvider.fetchPlaying();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
