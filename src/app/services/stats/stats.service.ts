import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Http } from '@angular/http';
import { conf } from '../../api.conf';

import { MenuItem } from '../../models/menu-item';


import 'rxjs/add/operator/map';

@Injectable()
export class StatsService {

  private logger = new Subject<Object>();

  currentMonthEarnings: number;
  relativeEarnings: number;
  topTenItems: Array<Object>;
  topTenSongs: Array<Object>;

  constructor(private http: Http) { }

  getStats(): Observable<Object> {
    return this.logger.asObservable();
  }

  filterStats(month: number) {
    this.fetchItemsStats(month);
  }

  fetchItemsStats(current: number) {
    this.http.get(`${conf.url}/Orders`)
      .map((response) => response.json())
      .subscribe((response) => {
        const currentMonth = current >= 0 ? current : new Date().getMonth();
        const currentMonthOrders = response.filter((o) => new Date(o.date).getMonth() == currentMonth);
        const lastMonthOrders = response.filter((o) => new Date(o.date).getMonth() == currentMonth - 1);

        this.currentMonthEarnings = currentMonthOrders.reduce((acc, i) => acc + i.total, 0);
        const lastMonthEarnings = lastMonthOrders.reduce((acc, i) => acc + i.total, 0);

        this.relativeEarnings = lastMonthEarnings ? this.currentMonthEarnings * 100 / lastMonthEarnings : 100;

        const entries = currentMonthOrders.reduce((acc, o) => {
          o.items.forEach((i) => {
            if (acc[i.name]) { acc[i.name]++ }
            else { acc[i.name] = 1; }
          });
          return acc;
        }, {});

        const topTenItems = Object.keys(entries)
          .map((key) => {return {name: key, quantity: entries[key]}})
          .sort((a, b) => b.quantity - a.quantity)
          .slice(0, 10);

        this.topTenItems = topTenItems;

        this.logger.next({
          currentMonthEarnings: this.currentMonthEarnings,
          relativeEarnings: this.relativeEarnings,
          topTenItems: this.topTenItems,
          topTenSongs: this.topTenSongs
        });
      });
  }

  fetchKaraokeStats() {
    this.http.get(`${conf.url}/Songs`)
      .map((response) => response.json())
      .subscribe((response) => {
        const entries = response.reduce((acc, s) => {
          if (acc[s.name]) { acc[s.name].quantity++; }
          else { acc[s.name] = {artist: s.artist, quantity: 1}; }
          return acc;
        }, {});
        const topTenSongs = Object.keys(entries).map((key) => {
          return {name: key, artist: entries[key].artist, quantity: entries[key].quantity};
        })
          .sort((a, b) => b.quantity - a.quantity)
          .slice(0, 10);

        this.topTenSongs = topTenSongs;
        this.logger.next({
          currentMonthEarnings: this.currentMonthEarnings,
          relativeEarnings: this.relativeEarnings,
          topTenItems: this.topTenItems,
          topTenSongs: this.topTenSongs
        });
      });
  }

}
