import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Http } from '@angular/http';
import { conf } from '../../api.conf';

import 'rxjs/add/operator/map';
import { Order } from '../../models/order';

@Injectable()
export class OrdersService {

  private logger = new Subject<Order[]>();
  orders: Order[];

  constructor(private http: Http) {
    this.orders = [];
    this.logger.next(this.orders);
  }

  getOrders(): Observable<Order[]> {
    return this.logger.asObservable();
  }

  filterOrders(from: string, to: string) {
    const fDate = new Date(from).getTime();
    const tDate = new Date(to).getTime();

    if (!from && !to) {
      this.logger.next(this.orders);
    } else if (from && to) {
      const filteredOrders = this.orders.filter((o) => {
        const oDate = new Date(o.date).getTime();
        return oDate >= fDate && oDate <= tDate
      });
      this.logger.next(filteredOrders);
    }
  }

  fetchOrders() {
    this.http.get(`${conf.url}/Orders`)
      .map((response) => response.json())
      .subscribe((response) => {
        const orders = response.map((o) => {
          const entries = o.items.reduce((acc, i) => {
            if (acc[i.name]) { acc[i.name]++; }
            else { acc[i.name] = 1; }
            return acc;
          }, {});
          const items = Object.keys(entries).map((key) => { return {name: key, quantity: entries[key]}});
          return new Order(o.id, o.table, o.client, o.total, o.date, items);
        });
        this.orders = orders;
        this.logger.next(this.orders);
      });
  }
}
