import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { conf } from '../../api.conf';

import 'rxjs/add/operator/map';
import { Song } from '../../models/song';
import { Timer } from '../../models/timer';

@Injectable()
export class KaraokeService {

  private queueLogger = new Subject<Song[]>();
  private playingLogger = new Subject<Song>();
  queue: Song[];
  played: Song[];
  playing: Song;
  timer: Timer;

  constructor(private http: Http, private router: Router) {
    this.queue = [];
    this.played = [];
    this.playing = new Song(null, null, null, null, null, null, null, null);
    this.queueLogger.next(this.queue);
  }

  getQueue(): Observable<Song[]> {
    return this.queueLogger.asObservable();
  }

  getPlaying(): Observable<Song> {
    return this.playingLogger.asObservable();
  }

  fetchQueue() {
    this.http.get(`${conf.url}/Songs`)
      .map((response) => response.json())
      .subscribe((response) => {
        const queue = response.slice(1).map((s) => {
          const client = {id: null, name: s.user, picture: s.picture};
          return new Song(s.id, client, s.name, s.artist, s.album, s.image, s.url, s.duration)
        });
        this.queue = queue;
        this.queueLogger.next(this.queue);
      });
  }

  fetchPlaying() {
    this.http.get(`${conf.url}/Songs/findOne`)
      .map((response) => response.json())
      .subscribe((s) => {
        const client = {id: null, name: s.user, picture: s.picture};
        const playing = new Song(s.id, client, s.name, s.artist, s.album, s.image, s.url, s.duration);
        this.playing = playing;
        this.playingLogger.next(this.playing);
      });
  }

  removeSong(id: number) {
    this.http.delete(`${conf.url}/Songs/${id}?access_token=${localStorage.getItem('authToken')}`)
      .map((response) => response.json())
      .subscribe((response) => {
        const index = this.queue.findIndex((s) => s.id == id);
        this.queue = [...this.queue.slice(0, index), ...this.queue.slice(index + 1)];
        this.queueLogger.next(this.queue);
      });
  }

  play() {
    const httpOptions = { headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('stfyToken')}`
    })};

    const body = this.timer ? null : {
      "uris": [this.playing.url],
    };

    this.http.put(`https://api.spotify.com/v1/me/player/play`, body, httpOptions)
      .map((response) => response.json())
      .subscribe(
        (response) => {
          if (!response.error) {
            if (!this.timer) this.timer = new Timer(() => this.playNext(), this.playing.duration);
            this.timer.start();
          } else {
            localStorage.removeItem('stfyToken');
            this.router.navigate(['/dashboard/bar-menu'])
          }
        },
        (error) => {
          localStorage.removeItem('stfyToken');
          this.router.navigate(['/dashboard/bar-menu'])
        }
      );
  }

  pause() {
    const httpOptions = { headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('stfyToken')}`
    })};

    this.http.put(`https://api.spotify.com/v1/me/player/pause`, null, httpOptions)
      .map((response) => response.json())
      .subscribe(
        (response) => {
          if (!response.error) {
            if (this.timer) this.timer.pause();
          } else {
            localStorage.removeItem('stfyToken');
            this.router.navigate(['/dashboard/bar-menu'])
          }
        },
        (error) => {
          localStorage.removeItem('stfyToken');
          this.router.navigate(['/dashboard/bar-menu'])
        }
      );
  }

  playNext() {
    if (this.queue.length) {
      // TODO: Make API call

      if (this.timer) this.timer.stop();
      this.timer = null;

      const current = this.playing;
      const next = this.queue.shift();

      this.played.push(current);
      this.playing = next;

      this.queueLogger.next(this.queue);
      this.playingLogger.next(this.playing);
    }
  }

}
