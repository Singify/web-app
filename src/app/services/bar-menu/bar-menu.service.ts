import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Http, Headers } from '@angular/http';
import { conf } from '../../api.conf';

import 'rxjs/add/operator/map';
import { MenuItem } from '../../models/menu-item';


@Injectable()
export class BarMenuService {

  private logger = new Subject<MenuItem[]>();
  items: MenuItem[];

  constructor(public http: Http) {
    this.items = [];
    this.logger.next(this.items);
  }

  getItems(): Observable<MenuItem[]> {
    return this.logger.asObservable();
  }

  fetchItems() {
    this.http.get(`${conf.url}/MenuItems`)
      .map((response) => response.json())
      .subscribe(
        (response) => {
          const items = response.map((i) => new MenuItem(i.id, i.name, i.description, i.price, i.type))
          this.items = items;
          this.logger.next(this.items);
        },
        (error) => console.log('An error occurred:', error.status)
      );
  }

  postItem(item: MenuItem) {
    const httpOptions = { headers: new Headers({ 'Content-Type': 'application/json' })};

    this.http.post(
      `${conf.url}/MenuItems?access_token=${localStorage.getItem('authToken')}`,
      item,
      httpOptions
    )
    .map((response) => response.json())
    .subscribe(
      (response) => {
        this.items.push(new MenuItem(response.id, response.name, response.description, response.price, response.type))
        this.logger.next(this.items);
      },
      (error) => console.log('An error occurred:', error.status)
    );
  }

  putItem(item: MenuItem) {
    const httpOptions = { headers: new Headers({ 'Content-Type': 'application/json' })};

    this.http.put(
      `${conf.url}/MenuItems/${item.id}?access_token=${localStorage.getItem('authToken')}`,
      item,
      httpOptions
    )
    .map((response) => response.json())
    .subscribe(
      (response) => {
        const item = new MenuItem(response.id, response.name, response.description, response.price, response.type);
        const index = this.items.findIndex((i) => {console.log(i.id, item.id); return i.id == item.id});
        this.items = [...this.items.slice(0, index), item, ...this.items.slice(index + 1)];
        this.logger.next(this.items);
      },
      (error) => console.log('An error occurred:', error.status)
    );
  }

  deleteItem(id: number) {
    this.http.delete(`${conf.url}/MenuItems/${id}?access_token=${localStorage.getItem('authToken')}`)
      .map((response) => response.json())
      .subscribe((response) => {
        const index = this.items.findIndex((i) => i.id == id);
        this.items = [...this.items.slice(0, index), ...this.items.slice(index + 1)];
        this.logger.next(this.items);
      });
  }

}
