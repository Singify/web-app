import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Http } from '@angular/http';
import { conf } from '../../api.conf';

import 'rxjs/add/operator/map';
import { Reservation } from '../../models/reservation';

@Injectable()
export class ReservationsService {

  private logger = new Subject<Reservation[]>();
  reservations: Reservation[];

  constructor(private http: Http) {
    this.reservations = [];
    this.logger.next(this.reservations);
  }

  getReservations(): Observable<Reservation[]> {
    return this.logger.asObservable();
  }

  filterReservations(filter: String) {
    if (filter) {
      const filteredReservations = this.reservations.filter((r) => r.date == filter);
      this.logger.next(filteredReservations);
    } else {
      this.logger.next(this.reservations);
    }
  }

  fetchReservations() {
    this.http.get(`${conf.url}/Reservations`)
      .map((response) => response.json())
      .subscribe((response) => {
        const reservations = response.map((r) => new Reservation(
          r.id, r.name,
          r.description, r.guests,
          r.attending, r.table,
          r.event, r.date,
          r.time, r.service
        ));
        this.reservations = reservations;
        this.logger.next(this.reservations);
      });
  }

  removeReservation(id) {
    this.http.delete(`${conf.url}/Reservations/${id}`)
      .map((response) => response.json())
      .subscribe((response) => {
        const index = this.reservations.findIndex((i) => i.id == id);
        this.reservations = [...this.reservations.slice(0, index), ...this.reservations.slice(index + 1)];
        this.logger.next(this.reservations);
      });
  }

}
