import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { conf } from '../../api.conf';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  private logger = new Subject<boolean>();
  isLogged: boolean;

  constructor(private router: Router, private http: Http) {
    this.isLogged = localStorage.getItem('authToken') ? true : false;
  }

  isAuthenticated(): Observable<boolean> {
    return this.logger.asObservable();
  }

  checkAuthState() {
    this.logger.next(this.isLogged);
  }

  authWithSpotify(token: string): boolean {
    if (token) {
      localStorage.setItem('stfyToken', token);
      return true;
    };
    return false;
  }

  doLogin(username: String, password: String) {
    const httpOptions = { headers: new Headers({ 'Content-Type': 'application/json' })};

    this.http.post(`${conf.url}/Managers/login`, {username, password}, httpOptions)
      .map((response) => response.json())
      .subscribe(
        (response) => {
          if (response.id) {
            localStorage.setItem('authToken', response.id);
            this.isLogged = true;
            this.logger.next(this.isLogged);
            this.router.navigate(['/dashboard/bar-menu']);
          }
        },
        (error) => console.log('An error occurred:', error.status)
      );
  }

  doLogout() {
    this.http.post(`${conf.url}/Managers/logout?access_token=${localStorage.getItem('authToken')}`, null)
      .subscribe(
        () => {
          localStorage.removeItem('authToken');
          this.isLogged = false;
          this.logger.next(this.isLogged);
          this.router.navigate(['/login']);
        },
        (error) => console.log('An error occurred:', error.status)
      );
  }

}
