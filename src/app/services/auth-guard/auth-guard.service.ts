import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  isLogged: boolean;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.authService.isAuthenticated().subscribe((a) => {
      this.isLogged = a;
    });
  }

  canActivate(): boolean {
    this.authService.checkAuthState();
    if (this.isLogged) return true;

    this.router.navigate(['/login']);
    return false;
  }

}
