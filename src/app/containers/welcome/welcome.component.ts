import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  fields: FormGroup;

  constructor(
    private auth: AuthService,
    private fb: FormBuilder
  ) {

    this.fields = fb.group({
      'username': [''],
      'password': ['']
    });
  }

  ngOnInit() {

  }

  onSubmit(value: any): void {
    console.log('Submitted the value: ', value);
    this.auth.doLogin(value.username, value.password);
  }

}
