import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
import { OrdersService } from '../../services/orders/orders.service';
import { Order } from '../../models/order';

@Component({
  selector: 'app-orders',
  templateUrl: 'orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  orders: Order[];

  constructor(private ordersProvider: OrdersService) {
    this.subscription = ordersProvider.getOrders().subscribe((o) => {
      this.orders = o;
    });
  }

  ngOnInit() {
    this.ordersProvider.fetchOrders();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onFilterChange(fromFilter: string, toFilter: string) {
    this.ordersProvider.filterOrders(fromFilter, toFilter);
  }

  formattedDate(date: string){
    return new Date(date).toLocaleDateString("en-US", { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric', timeZone: 'UTC' });
  }

}
