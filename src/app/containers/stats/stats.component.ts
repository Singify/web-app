import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
import { StatsService } from '../../services/stats/stats.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  stats: object;
  currentMonth: number;

  constructor(private statsProvider: StatsService) {
    this.currentMonth = new Date().getMonth() + 1;
    this.subscription = statsProvider.getStats().subscribe((s) => {
      this.stats = s;
    });
  }

  filterStats(month: number) {
    this.statsProvider.filterStats(month - 1);
  }

  ngOnInit() {
    this.statsProvider.fetchItemsStats(this.currentMonth - 1);
    this.statsProvider.fetchKaraokeStats();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
