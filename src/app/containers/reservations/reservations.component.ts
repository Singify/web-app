import { Component, OnInit , OnDestroy} from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
import { ReservationsService } from '../../services/reservations/reservations.service';
import { Reservation } from '../../models/reservation';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  reservations: Reservation[];

  constructor(private reservationsProvider: ReservationsService) {
    this.subscription = reservationsProvider.getReservations().subscribe((r) => {
      this.reservations = r;
    });
  }

  ngOnInit() {
    this.reservationsProvider.fetchReservations()
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onFilterChange(filter: string) {
    this.reservationsProvider.filterReservations(filter);
  }

  formattedDate(date: string){
    return new Date(date).toLocaleDateString("en-US", { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric', timeZone: 'UTC' });
  }

  removeReservation(id: String) {
    this.reservationsProvider.removeReservation(id);
  }

}
