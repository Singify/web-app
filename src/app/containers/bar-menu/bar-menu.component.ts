import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";

import { BarMenuService } from '../../services/bar-menu/bar-menu.service';
import { MenuItem } from '../../models/menu-item';

enum MenuItemTypes {
  Default,
  Drink,
  Snack,
  Food
};

@Component({
  selector: 'app-bar-menu',
  templateUrl: './bar-menu.component.html',
  styleUrls: ['./bar-menu.component.scss']
})
export class BarMenuComponent implements OnInit, OnDestroy {

  subscription: ISubscription;
  isCreating: Boolean;
  items: MenuItem[];
  formItem: MenuItem;

  constructor(private menuProvider: BarMenuService) {
    this.isCreating = true;
    this.formItem = new MenuItem(null, null, null, null, String(MenuItemTypes['Default']));
    this.subscription = menuProvider.getItems().subscribe((i) => {
      this.items = i;
    });
  }

  ngOnInit() {
    this.menuProvider.fetchItems();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toggleItem(item) {
    this.isCreating = false;
    this.formItem.id = item.id;
    this.formItem.name = item.name;
    this.formItem.type = MenuItemTypes[item.type];
    this.formItem.price = item.price;
    this.formItem.description = item.description;
  }

  clearFormItem() {
    this.formItem.id = null;
    this.formItem.name = null;
    this.formItem.type = String(MenuItemTypes['Default']);
    this.formItem.price = null;
    this.formItem.description = null;
    this.isCreating = true;
  }

  postItem() {
    let newItem = new MenuItem(this.formItem.id, this.formItem.name, this.formItem.description, this.formItem.price, MenuItemTypes[this.formItem.type]);
    this.menuProvider.postItem(newItem);
    this.clearFormItem();
  }

  putItem() {
    let upadtedItem = new MenuItem(this.formItem.id, this.formItem.name, this.formItem.description, this.formItem.price, MenuItemTypes[this.formItem.type]);
    this.menuProvider.putItem(upadtedItem);
    this.clearFormItem();
  }

  removeItem(id: number) {
    this.menuProvider.deleteItem(id);
    this.clearFormItem();
  }

}
