import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-dashboard',
  template: `
    <div class="Dashboard o-grid">
      <!-- Dashboard working area -->
      <div class="Dashboard__working-area [ o-grid__child u-12of12 ]">
        <router-outlet></router-outlet>
      </div>

      <!-- Dashboard widgets navigation -->
      <div class="Dashboard__widgets [ o-grid__child u-12of12 ] [ o-grid ]">
        <player-widget *ngIf="!state.showKaraokeWidget" class="o-grid__child u-12of12 u-6of12@tab"></player-widget>
        <karaoke-widget *ngIf="state.showKaraokeWidget" class="o-grid__child u-12of12 u-6of12@tab"></karaoke-widget>
        <nav-widget class="o-grid__child u-12of12 u-6of12@tab"></nav-widget>
      </div>
    </div>
  `,
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  state: Object;
  routeSubscription: ISubscription;

  constructor(private router:Router) {
    this.state = {
      showKaraokeWidget: router.url.includes('/karaoke')
    }

    this.routeSubscription = router.events.subscribe((val) => {
      if (val instanceof NavigationStart) {
        this.state = Object.assign({}, this.state, {showKaraokeWidget: val.url.includes('/karaoke')});
      }
    });
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

}
