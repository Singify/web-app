import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { BarMenuComponent } from '../bar-menu/bar-menu.component';
import { ReservationsComponent } from '../reservations/reservations.component';
import { OrdersComponent } from '../orders/orders.component';
import { PlayerComponent } from '../../components/player/player.component';
import { StatsComponent } from '../stats/stats.component';

import { AuthGuard } from '../../services/auth-guard/auth-guard.service';

const dashboardRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'bar-menu', component: BarMenuComponent },
      { path: 'reservations', component: ReservationsComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'karaoke', component: PlayerComponent },
      { path: 'stats', component: StatsComponent },
      { path: '', redirectTo: '/dashboard/bar-menu', pathMatch: 'full' },
      { path: '**', redirectTo: '/dashboard/bar-menu'}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      dashboardRoutes,
      { enableTracing: false }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule {}
