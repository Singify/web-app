import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { DashboardRoutingModule } from './containers/dashboard/dashboard-routing.module';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './containers/welcome/welcome.component';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { PlayerWidgetComponent } from './components/player-widget/player-widget.component';
import { NavWidgetComponent } from './components/nav-widget/nav-widget.component';
import { BarMenuComponent } from './containers/bar-menu/bar-menu.component';
import { ReservationsComponent } from './containers/reservations/reservations.component';
import { OrdersComponent } from './containers/orders/orders.component';
import { PlayerComponent } from './components/player/player.component';
import { KaraokeWidgetComponent } from './components/karaoke-widget/karaoke-widget.component';
import { ParticipantComponent } from './components/participant/participant.component';
import { StatsComponent } from './containers/stats/stats.component';

import { HttpModule } from '@angular/http';

import { AuthService } from './services/auth/auth.service';
import { BarMenuService } from './services/bar-menu/bar-menu.service';
import { ReservationsService } from './services/reservations/reservations.service';
import { OrdersService } from './services/orders/orders.service';
import { KaraokeService } from './services/karaoke/karaoke.service';
import { StatsService } from './services/stats/stats.service';

import { AuthGuard } from './services/auth-guard/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    DashboardComponent,
    PlayerWidgetComponent,
    NavWidgetComponent,
    BarMenuComponent,
    ReservationsComponent,
    OrdersComponent,
    PlayerComponent,
    KaraokeWidgetComponent,
    ParticipantComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardRoutingModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    BarMenuService,
    ReservationsService,
    OrdersService,
    KaraokeService,
    StatsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
