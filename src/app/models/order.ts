export class Order {
  id: number;
  table: number;
  client: string;
  total: number;
  date: string;
  items: any

  constructor (
    id: number,
    table: number,
    client: string,
    total: number,
    date: string,
    items: any
  ) {
    this.id = id;
    this.table = table;
    this.client = client;
    this.total = total;
    this.date = date;
    this.items = items;
  }
}
