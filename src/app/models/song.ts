export class Song {
  id: string | number;
  client: {
    id: number,
    name: string,
    picture: string
  };
  name: string;
  artist: string;
  album: string;
  image: string;
  url: string;
  duration: number;

  constructor(
    id: number | string,
    client: any,
    name: string,
    artist: string,
    album: string,
    image: string,
    url: string,
    duration: number
  ) {
    this.id = id;
    this.client = client;
    this.name = name;
    this.artist = artist;
    this.album = album;
    this.image = image;
    this.url = url;
    this.duration = duration;
  }

}
