export class MenuItem {
  id: number;
  name: string;
  description: string;
  price: number;
  type: string;

  constructor(
    id: number,
    name: string,
    description: string,
    price: number,
    type: string) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.price = price;
      this.type = type;
    }
}
