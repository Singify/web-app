export class Reservation {
  id: number;
  name: string;
  description: string;
  guests: number;
  attending: number;
  table: number;
  event: string;
  date: string;
  time: string;
  service: string;

  constructor(
    id: number,
    name: string,
    description: string,
    guests: number,
    attending: number,
    table: number,
    event: string,
    date: string,
    time: string,
    service: string
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.guests = guests;
    this.attending = attending;
    this.table = table;
    this.event = event;
    this.date = date;
    this.time = time;
    this.service = service;
  }
}
