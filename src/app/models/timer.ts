export class Timer {
  id: number;
  init: number;
  remaining: number;
  callback: Function;

  constructor(callback, delay) {
    this.callback = callback;
    this.remaining = delay;
  }

  start() {
    this.init = new Date().getTime();
    window.clearTimeout(this.id);
    this.id = window.setTimeout(this.callback, this.remaining);
  }

  pause() {
    window.clearTimeout(this.id);
    this.remaining = this.remaining - (new Date().getTime() - this.init);
  }

  stop() {
    window.clearTimeout(this.id);
  }
}
